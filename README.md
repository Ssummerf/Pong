# Simple GBA Pong

A very simple C program to be run on the GBA. The majority of the difficulty 
involved was integrating into GBA controls, though the game does work with a 
GBA emulator on the PC as well.

Features:

```
A visual display, and controls fitting for the GBA
Hitboxes for each Pong Paddle
A simple AI to play against
A tallying scoreboard
```
